from Point import Point
from Polygon import Polygon
import json

try:
	with open('Inputs/convex_input.json') as json_file: 
		data = json.load(json_file) # load data
		for vertices in data: # for every verices
			hull = Polygon(0) #default zero
			hull.giftWrap2D(vertices, data[vertices])
			if (hull.checkConvex()):
				print("Convex Hull:")
				hull.points.printList()
			else:
				print("Some Error occured")

except Exception as e:
	print("Exception caught: " + repr(e))
