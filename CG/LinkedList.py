class Node:
	def __init__(self, data = None):
		self.data = data
		self.next = None
		self.prev = None

class LinkedList:
	def __init__(self):
		self.head = None

	def push(self, val):
		newNode = Node(val)

		if (self.head is None):
			self.head = newNode
			newNode.next = self.head
			newNode.prev = self.head
			return

		newNode.prev = self.head.prev
		newNode.next = self.head
		self.head = newNode

	def append(self, val):
		newNode = Node(val)
		#on empty list
		if (self.head is None):
			self.head = newNode
			newNode.next = self.head
			newNode.prev = self.head
			return

		#on non empty lists
		last = self.head
		while(last.next is not self.head):
			last = last.next

		newNode.next = last.next
		newNode.prev = last
		last.next = newNode
		return

	def printList(self):
		last = self.head
		while(last.next is not self.head):
			print(last.data.get())
			last = last.next
		print(last.data.get())
