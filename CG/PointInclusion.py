from Point import Point
from Polygon import Polygon
import json

try:
	with open('Inputs/inclusion_input.json') as json_file: 
	    data = json.load(json_file) # load data
	    for vertices in data: # for every verices 
	    	length = int(vertices)

	    	if (length < 3): # raise error if less number of veritces
	    		raise Exception("Minimum length 3")
	    	poly = Polygon(length) # create polygon class
	    	poly.getPoints(data[vertices]['points']) # set polygon values from json file

	    	for point in data[vertices]['check']:
		    	p = Point()
		    	p.set(int(point['x']), int(point['y']))
    			print("\nFor points: ")
				poly.points.printList()
		    	inclusion = poly.checkPointInclusion(p)
		    	print(f"for point {p.get()}")
		    	if inclusion is True:
		    		print("Inclusion of point in Polygon")
		    	else:
	    			print("Non convex at point " + str(inclusion[0]) + ", " + str(inclusion[1]))
except Exception as e:
	print("Exception caught: " + repr(e))
