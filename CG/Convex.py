from Point import Point
from Polygon import Polygon
import json

try:
	with open('Inputs/convex_input.json') as json_file: 
		data = json.load(json_file) # load data
		for vertices in data: # for every verices 
			length = int(vertices)

			if (length <= 3): # raise error if less number of veritces
				raise Exception("Minimum length 3")
			poly = Polygon(length) # create polygon class
			poly.getPoints(data[vertices]) # set polygon values from json file

			print("For points: ")
			poly.points.printList()

			convex = poly.checkConvex() # check for convexity

			if convex is True:
				print("Convex Polygon")
			else:
				print("Non Convex Polygon at point " + str(convex[0]) + ", " + str(convex[1]))

			print("\n")
except Exception as e:
	print("Exception caught: " + repr(e))

