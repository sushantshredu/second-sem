from Point import Point
from Line import Line

def getPoints():
	x = int(input("Enter x coordinate:"))
	y = int(input("Enter y coordinate:"))

	return x, y

#First Point
p1 = Point()
print('Enter first point:')
x, y = getPoints()
p1.set(x, y)

#Second Point
p2 = Point()
print('Enter second point:')
x, y = getPoints()
p2.set(x, y)

#Creating a line
line = Line()
line.set(p1, p2)

#Point to check for dimension
p = Point()
print("Enter the point to check:")
x, y = getPoints()
p.set(x, y)

try :
	print("Between: %s" % line.checkBetween(p))
	print("Behind: %s" % line.checkBehind(p))
	print("Beyond: %s" % line.checkBeyond(p))
	print("Start: %s" % line.checkStart(p))
	print("Terminous: %s" % line.checkTerminous(p))
except Exception as e:
	print('Caught this error: ' + repr(e))



