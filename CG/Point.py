class Point:
	def getPoints(self):
		x = int(input("\nEnter x coordinate:"))
		y = int(input("Enter y coordinate:"))

		self.x = x
		self.y = y

	def set(self, x, y):
		self.x = x
		self.y = y

	def getX(self):
		return self.x

	def getY(self):
		return self.y

	def get(self):
		return self.x, self.y

	def checkTurn(self, p2, p3):
		area = self.computeArea(p2, p3)
		if area > 0:
			return 1 #left
		elif area < 0:
			return -1 #right
		else:
			return 0 #collinear

	def computeArea(self, p2, p3):
		down = self.x * p2.y + self.y * p3.x + p2.x * p3.y
		up = self.y * p2.x + self.x * p3.y + p2.y * p3.x

		return (float(down) - float(up) )/2

	def checkIntersection(self, p2, p3, p4):
		if (self.checkTurn(p2, p3) is 0 or self.checkTurn(p2, p4) is 0):
			return 1
		if (
			(self.checkTurn(p2, p3) != self.checkTurn(p2, p4)) and 
			(p3.checkTurn(p4, self) != p3.checkTurn(p4, p4) is 1)
			):
			return 1
		else:
			return 0