from Point import Point

class Line:
	def set(self, p1, p2):
		self.p1 = p1
		self.p2 = p2

	def getFirstPoint(self):
		return self.p1.get()

	def getSecondPoint(self):
		return self.p2.get()

	def checkBetween(self, p):
		check = self.p1.checkTurn(self.p2, p)
		if not (check is 0) :
			return False
		if ((self.p1.getX() < p.x and self.p2.getX() > p.x) or (self.p1.getY() < p.y and self.p2.getY() > p.y)):
			return True
		return False

	def checkBehind(self, p):
		check = self.p1.checkTurn(self.p2, p)
		if not (check is 0) :
			raise Exception("Points not collinear")
		if (self.p1.getX() > p.x or self.p1.getY() > p.y):
			return True
		return False

	def checkBeyond(self, p):
		check = self.p1.checkTurn(self.p2, p)
		if not (check is 0) :
			raise Exception("Points not collinear")
		if (self.p2.getX() < p.x or self.p2.getY() < p.y):
			return True
		return False

	def checkStart(self, p):
		if self.p1.getX() == p.x and self.p1.getY() == p.y:
			return True
		return False

	def checkTerminous(self, p):
		if self.p2.getX() == p.x and self.p2.getY() == p.y:
			return True
		return False

	def checkIntersection(self, l2):
		if (
			(self.p1.checkTurn(self.p2, l2.p1) is 0 and self.checkBetween(l2.p1)) or
			(self.p1.checkTurn(self.p2, l2.p2) is 0 and self.checkBetween(l2.p2)) or
			(l2.p1.checkTurn(l2.p2, self.p1) is 0 and l2.checkBetween(self.p1)) or
			(l2.p1.checkTurn(l2.p2, self.p1) is 0 and l2.checkBetween(self.p2))
			):
			return True
		if (
			(self.p1.checkTurn(self.p2, l2.p1) != self.p1.checkTurn(self.p2, l2.p2)) and 
			(l2.p1.checkTurn(l2.p2, self.p1) != l2.p1.checkTurn(l2.p2, self.p2))
			):
			return 1
		else:
			return 0